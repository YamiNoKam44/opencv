#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
using namespace cv;
using namespace std;
//width: 640, height:480
String position(int width, int height) {
	String positionH;
	if (height < 200) {
		if(width < 150)
			return "top left";
		if(width > 150 && width < 330)
			return "top center";
		if(width > 330)
			return "top right";
	}
	else if (height > 200 && height < 440) {
		if (width < 150)
			return "center left";
		if (width > 150 && width < 330)
			return "center";
		if (width > 330)
			return "center right";
	}
	else if (height > 440) {
		if (width < 150)
			return "bottom left";
		if (width > 150 && width < 330)
			return "bottom center";
		if (width > 330)
			return "bottom right";
	}
}
int main(int argc, char** argv)
{
	VideoCapture cap(0); //pobieranie wideo z kamery

	if (!cap.isOpened())  // W wypadku braku obrazu zaka�czamy ca�y proces
	{
		cout << "Nie mo�na otworzy� obrazu z kamery / brak kamery" << endl;
		return -1;
	}

	//Zapis obrazu z kamery na tymczasowy obraz
	Mat imgTmp;
	cap.read(imgTmp);
	namedWindow("Control", CV_WINDOW_AUTOSIZE); //tworzenie panelu kontrolnego zwanego "Control"

	int iLowH = 0;
	int iHighH = 179;

	int iLowS = 116;
	int iHighS = 255;

	int iLowV = 171;
	int iHighV = 255;

	//Menu trackballi z okna "Control"
	createTrackbar("LowH", "Control", &iLowH, 179); //Hue (0 - 179)
	createTrackbar("HighH", "Control", &iHighH, 179);

	createTrackbar("LowS", "Control", &iLowS, 255); //Saturation (0 - 255)
	createTrackbar("HighS", "Control", &iHighS, 255);

	createTrackbar("LowV", "Control", &iLowV, 255);//Value (0 - 255)
	createTrackbar("HighV", "Control", &iHighV, 255);

	int iLastX = -1;
	int iLastY = -1;

	//Wytworzenie czarnego [szum�w] obrazka z parametrami kamery
	Mat imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);


	while (true)
	{
		Mat imgOriginal;

		bool bSuccess = cap.read(imgOriginal); // Odczytaj przechwycony obraz z wideo kamery



		if (!bSuccess) //W wypadku problemu z przechwyceniem obrazu - przerwij p�tle
		{
			cout << "Nie mo�na odczyta� obrazu z video kamery" << endl;
			break;
		}

		Mat imgHSV;
		cvtColor(imgOriginal, imgHSV, COLOR_BGR2HSV); //Zamiana obrazu z BRG na HSV

		Mat imgThresholded;//obraz szum�w

		inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded); //Threshold the image

		 //Otwarcie morphological (usuwa ma�e obiekty z pierwszego planu)
		erode(imgThresholded, imgThresholded, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
		dilate(imgThresholded, imgThresholded, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));

		//Zamkni�cie morphological (usuwa ma�e dziury z pierwszego planu)
		dilate(imgThresholded, imgThresholded, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));
		erode(imgThresholded, imgThresholded, getStructuringElement(CV_SHAPE_ELLIPSE, Size(3, 3)));

		//Wykalkuluj moment progu zdj�cia
		Moments oMoments = moments(imgThresholded);

		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;

		// Je�eli szumy s� powy�ej warto�ci 5000, to rysuj warto�ci
		if (dArea > 5000)
		{
			//wykalkuluj pozycj� pi�ki 
			int posX = dM10 / dArea;
			int posY = dM01 / dArea;
			printf("Area X: %d \nArea Y: %d \n", posX,posY);
			printf("Pozycja to: %s \n", position(posX, posY).c_str());

			if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
			{
				//Narysuj niebiesk� linie

				line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(255, 0, 0), 2);
			}

			iLastX = posX;
			iLastY = posY;

		}

		imshow("Thresholded Image", imgThresholded); //poka� zdj�cie progowe (w szumach)

		imgOriginal = imgOriginal + imgLines;
		imshow("Original", imgOriginal); //pokarz orginalny obrazek

		if (waitKey(30) == 27) //ESC zaka�cza program
		{
			cout << "Klawisz Esc zaka�czaj�cy program" << endl;
			break;
		}
	}

	return 0;
}